package org.aop.javaconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainApp {
	
	
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
		Performance performance = (Performance) context.getBean("dancePerformance");
		performance.perform();
		performance.perform(4);
		//performance has been casted to Encorable and encore method is called on it.
		Encorable encore = (Encorable) performance;
		encore.encore();
		//casting performance back to Performance type and calling perform method
		Performance performance1 = performance;
		performance1.perform();
	}
}
