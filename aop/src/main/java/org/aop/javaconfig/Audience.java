package org.aop.javaconfig;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.DeclareParents;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Audience {
    
    	@DeclareParents(value = "org.aop.javaconfig.Performance+", defaultImpl = EncoreImpl.class)
    	public static Encorable encore;
    

	@Pointcut("execution(* Performance.perform(..))")
	public void performance() {
	}

	/*
	 * @Before("execution(* Performance.perform(..))") public void
	 * silenceCellPhones() { System.out.println("silencing cell phones"); }
	 * 
	 * @Before("execution(* Performance.perform(..)) && bean(dancePerformance)")
	 * public void takeSeats() { System.out.println("taking seats"); }
	 * 
	 * @AfterReturning("performance()") //using the common pointcut defined
	 * public void applause() { System.out.println("applauding"); }
	 * 
	 * @AfterThrowing("execution(* Performance.perform(..))") public void
	 * demandRefund() { System.out.println("demanding a refund"); }
	 */

	// around advice can be used to do what both before and after does
	@Around("performance()")
	public void watchPerformance(ProceedingJoinPoint jp) {
		try {
			System.out.println("silencing cell phones");
			System.out.println("taking seats");
			jp.proceed();
			System.out.println("clap clap clap");
		} catch (Throwable e) {

		}

	}

	// args is used to pass the argument passed to the method to be passed in
	// the advice. It is useful while calling the proceed method in the around
	// advice
	@Before("execution(* Performance.perform(int)) && args(a)")
	public void silenceCellPhones(int a) {
		System.out.println("silencing cell phones for : " + a);
		encore.encore();
	}

}
