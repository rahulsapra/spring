package org.aop.javaconfig;

import org.springframework.stereotype.Component;

@Component
public interface Performance {
	public void perform();
	public void perform(int a);
}
