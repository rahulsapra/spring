package org.aop.javaconfig;

import org.springframework.stereotype.Component;

@Component
public class DancePerformance implements Performance{

	@Override
	public void perform() {
		// TODO Auto-generated method stub
		System.out.println("a dance performance");
	}

	@Override
	public void perform(int a) {
		System.out.println("dance performance: "+a);
		
	}

}
