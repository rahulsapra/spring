package org.aop;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component // we need to explicitly create the bean for it to be used as aspect
public class MyAspect {

    // since the aspect-autoproxy is declared in beans.xml this class will be
    // treated as an aspect
    @Before("execution(* *.embarkOnQuest(..))")
    public void doSomething() {
	System.out.println("doing something");
    }
    
    @AfterReturning("execution(* *.embarkOnQuest(..))")
    public void doSomethingBeforeReturning() {
	System.out.println("doing something before returning");
    }
    

}
