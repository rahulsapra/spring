package org.aop;

import org.aspectj.lang.annotation.Aspect;


public class Minstrel {

	public void singBeforeQuest(){
		System.out.println("singing before quest");
	}
	
	public void singAfterQuest(){
		System.out.println("quest has been completed successfully");
	}
}
