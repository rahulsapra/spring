package org.propertiesfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


@PropertySource("application.properties")
public class AppConfig {

	
	@Autowired
	Environment environment;
	
	@Bean
	public Person person(){
		return new Person(environment.getProperty("firstname"),
						environment.getProperty("lastname"));
	}
}
