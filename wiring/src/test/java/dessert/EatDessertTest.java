package dessert;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DessertConfig.class)
public class EatDessertTest {

	@Rule
	public final StandardOutputStreamLog log = new StandardOutputStreamLog();
	
	@Autowired
	private Dessert dessert;
	
	@Autowired
	private EatDessert eatDessert;
	
	@Test
	public void testDessertNull() {
		assertNotNull(dessert);
	}
	
	@Test
	public void testGetDetails(){
		eatDessert.getDetails();
		assertEquals("ice cream",log.getLog());
		System.out.println("demo");
	}
	
}
