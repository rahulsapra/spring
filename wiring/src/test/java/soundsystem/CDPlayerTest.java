package soundsystem;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=CDPlayerConfig.class)
public class CDPlayerTest {

	@Rule
	public final StandardOutputStreamLog log = new StandardOutputStreamLog();
	
	@Autowired
	CDPlayer player;
	
	@Autowired
	private CompactDisc cd;
	
	@Test
	public void testCdShouldNotBeNull() {
		assertNotNull(cd);
	}
	
	@Test
	public void testPlay(){
		player.play();
		assertEquals("playing my song by artist",log.getLog());
		System.out.println("demo");
	}

}
