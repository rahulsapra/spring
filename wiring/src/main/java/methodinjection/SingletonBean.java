package methodinjection;

/**
 * 
 * lookup method injection is used when a singleton object has reference to a
 * prototype class in this case SingletonBean class has reference to
 * PrototypeBean class
 *
 */

public abstract class SingletonBean {
	protected abstract PrototypeBean createPrototypeBean();

	public void doSomething() {
		createPrototypeBean().doSomething();
	}

	public void doSomethingElse() {
		createPrototypeBean().doSomethingElse();
	}

}
