package methodinjection;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LookupMethodInjectionApp {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"MethodInjectionBeans.xml"});
		SingletonBean singleton = (SingletonBean) context.getBean("singleton");
		singleton.doSomething();
		singleton.doSomethingElse();
	}
}	
