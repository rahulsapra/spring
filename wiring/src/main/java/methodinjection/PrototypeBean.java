package methodinjection;

import java.util.concurrent.SynchronousQueue;

public class PrototypeBean {
	public void doSomething() {
		System.out.println("doing something");
	}

	public void doSomethingElse() {
		System.out.println("doing something else");
	}

}
