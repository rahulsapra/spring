package methodinjection.methodReplacer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MethodReplacerApp {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("MethodReplacerBeans.xml");
		A a = (A) context.getBean("a");
		System.out.println(a.print("hello world"));
	}
}
