package methodinjection.methodReplacer;

import java.lang.reflect.Method;

import org.springframework.beans.factory.support.MethodReplacer;

public class Replacer implements MethodReplacer{
	public void doSomething() {
		System.out.println("doing something inside Replacer");
	}

	public Object reimplement(Object arg0, Method arg1, Object[] arg2) throws Throwable {
		String str = (String) arg2[0];
		return str;
	}
}
