package dessert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EatDessert {

	@Autowired
	private Dessert dessert;
	
	public void getDetails() {
		dessert.getDetails();
	}
}
