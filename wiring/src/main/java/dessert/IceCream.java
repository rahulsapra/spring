package dessert;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Primary
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//session and requst scope are present in WebApplicationContext 
public class IceCream implements Dessert {

	public void getDetails() {
		System.out.print("ice cream");
	}

}
