package dessert;

import org.springframework.stereotype.Component;

@Component
public class Fruit implements Dessert {

	public void getDetails() {
		System.out.print("fruit");
	}

}
