package soundsystem;

import org.springframework.stereotype.Component;

//giving custom id to the bean
@Component("mySgtPeppersBean")
public class SgtPeppers implements CompactDisc {

	private String title = "my song";
	private  String artist = "artist";
	
	public void play() {
		System.out.print("playing "+title+" by "+artist);
	}

}
