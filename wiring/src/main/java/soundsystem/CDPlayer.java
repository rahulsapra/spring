package soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CDPlayer implements MediaPlayer {

	private CompactDisc cd;
	
	/*
	 *	autowired annotation will automatically search for a CompactDisc bean alreafy
	 *	created and pass it to the constructor as argument. 
	 * @param cd
	 */
	@Autowired
	public CDPlayer(CompactDisc cd) {
		this.cd = cd;
	}
	
	
	@Autowired
	public void insertDisc(CompactDisc cd){
		this.cd = cd;
	}
	
	/*
	 * setting the required attribute to false will ensure no exception is thrown when 
	 * spring could not find any bean during application context creation 
	 * @param cd
	 */
//	@Autowired(required=false)
	public void setCompactDisc(CompactDisc cd){
		this.cd = cd;
	}
	
	
	public void play() {
		cd.play();
	}

}
