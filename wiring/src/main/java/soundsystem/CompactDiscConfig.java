package soundsystem;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * This is a configuration class to tell Spring to create beans for the classes that are marked with
 * @Component annotation 
 * @author rahul
 *
 */
@Configuration
@ComponentScan
public class CompactDiscConfig {
}
