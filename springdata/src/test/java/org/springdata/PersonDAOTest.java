package org.springdata;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class)
public class PersonDAOTest {

	@Autowired
	Person person;
	
	@Autowired
	PersonDAO personDAO;
	
	@Test
	public void testPersonNotNull(){
		assertNotNull(person);
	}
	
	@Test
	public void testPersonDAONotNull(){
		assertNotNull(personDAO);
	}
	
	
	@Test
	public void testSavePerson(){
		System.out.println(personDAO.jdbcRespository);
		person.setFirstName("fname");
		person.setLastName("lname");
		personDAO.savePerson(person);
	}
}
