package org.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcRespository implements MyRepository{

	private JdbcOperations jdbcOperations;
	
	@Autowired
	public JdbcRespository(JdbcTemplate jdbcTemplate) {
		this.jdbcOperations = jdbcTemplate;
	}


	public JdbcOperations getJdbcOperations() {
		return jdbcOperations;
	}
	

}
