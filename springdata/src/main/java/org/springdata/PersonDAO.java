package org.springdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonDAO {

	@Autowired
	JdbcRespository jdbcRespository;
	
	
	public void savePerson(Person person){
//		jdbcRespository.getJdbcOperations().execute("insert into Person values ("+
//				person.getId()+","+person.getFirstName()+","+person.getLastName()+")");
		jdbcRespository.getJdbcOperations().execute("Insert into person values(5,\'fname\',\'lname\');");
		System.out.println("person added");
	}
}
