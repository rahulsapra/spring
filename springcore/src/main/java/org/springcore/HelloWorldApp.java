package org.springcore;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloWorldApp {
	public static void main(String[] args) {
		//looks for file in the classpath
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("HelloWorldBeans.xml");
		//requires the complete path of the file
//		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("C:/Users/rahul/git/spring/springcore/src/main/resources/HelloWorldBeans.xml");	
		HelloWorld object = (HelloWorld) context.getBean("helloWorld");
		System.out.println(object.getMessage());
		context.close();
	}
}
