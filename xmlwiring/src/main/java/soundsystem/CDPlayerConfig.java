package soundsystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * This is a configuration class to tell Spring to create beans for the classes that are marked with
 * @Component annotation 
 * @author rahul
 *
 */
@Configuration
//@ComponentScan(basePackages="soundsystem")
public class CDPlayerConfig {
	
	/*
	 * if we don't put ComponentScan annotation on the java config class
	 * we need to explicitly create the beans
	 */
	
	@Bean(name = "mySgtPeppers")//custom id to the bean
	public CompactDisc sgtPeppers(){
		return new SgtPeppers();
	}
	
	@Bean
	public MediaPlayer cdPlayer(CompactDisc cd){
//		return new CDPlayer(cd);
		return new CDPlayer(new SgtPeppers());
	}
	
	
}
